import csv
import sys
import itertools

if __name__ == '__main__':
    result = {}
    versions = set()

    with open(sys.argv[1]) as f:
        reader = csv.reader(f, delimiter=',')

        for data in reader:
            # Skip first values.
            skip = False

            if data[0].startswith("#"):
                skip = True

            if data[0] == "date":
                skip = True

            if skip:
                continue

            date = data[0]
            version = data[1]
            count = data[2]

            versions.add(version)

            if date not in result:
                result[date] = {}

            result[date][version] = count

    line = "date"

    for version in sorted(versions):
        line += ",{}".format(version)

    print(line)

    for date in sorted(result.keys()):
        line = "{}".format(date)

        for version in sorted(versions):
            line += ",{}".format(result[date].get(version, 0))

        print(line)
