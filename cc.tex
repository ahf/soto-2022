%% Copyright (c) 2022 The Tor Project, inc. All rights reserved.
%% Use of this source code is governed by a BSD-style
%% license that can be found in the LICENSE file.

\begin{frame}
    \frametitle{Congestion Control}

    We implemented three congestion control algorithms: Tor-Westwood,
    Tor-Vegas, and Tor-NOLA. All of them are available in \highlight{Tor
    0.4.7.}

    Both Tor-Westwood and Tor-NOLA exhibited ack compression, which
    caused them to wildly overestimate the Bandwidth-Delay Product, which
    lead to runaway congestion conditions.

    Google's BBR algorithm also suffers from these problems, and was not
    implemented in Tor.
\end{frame}

\begin{frame}
    \frametitle{Congestion Control}

    \highlight{Tor-Vegas} performed beautifully, almost exactly as the theory
    predicted, as seen in the results from \highlight{Shadow.}

    \begin{center}
        \includegraphics[width=0.9\textwidth]{images/shadow.png}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Congestion Control}
    \centering
    \begin{tikzpicture}
        \begin{axis}[
                title=Total Relay Bandwidth,
                title style={font=\scriptsize\bfseries},
                no markers,
                enlarge x limits=false,
                grid=both,
                grid style=dashed,
                width=0.85\paperwidth,
                height=0.80\paperheight,
                date coordinates in=x,
                xmin=2021-01-01,
                xmax=2022-08-01,
                xtick={
                    {2021-01-01},
                    {2022-01-01},
                    {2023-01-01}
                },
                cycle list name=exotic,
                every axis plot/.append style={thick},
                label style={font=\scriptsize},
                tick label style={font=\scriptsize},
                legend style={
                    font=\tiny,
                },
                legend pos=north west,
                legend cell align=left,
                unbounded coords=discard,
                xticklabel style={
                    anchor=near xticklabel,
                },
                ylabel={Bandwidth in Gbit/s},
                extra x tick style={xticklabel=\year},
                xticklabel=\year\
                ]

            \addlegendentry{Advertised Bandwidth}
            \addplot table [x=date, y=advbw, col sep=comma, emphasize=2022-04-24:2023-01-01 with OnionPurple!30] {data/bandwidth-flags_compressed.csv};

            \addlegendentry{Historic Bandwidth}
            \addplot table [x=date, y=bwhist, col sep=comma] {data/bandwidth-flags_compressed.csv};
        \end{axis}
    \end{tikzpicture}

    \tiny Source: \href{https://metrics.torproject.org/}{metrics.torproject.org}
\end{frame}

\begin{frame}
    \frametitle{Ongoing Denial of Service}

    The ongoing Denial of Service against the Tor network in the last couple of
    months have made it drastically harder to analyse the impact and tuning
    opportunities related to the deployment of congestion control in the
    network.

    Ongoing efforts to reduce the impact of Denial of Service attacks is
    helping, but it continues to be a bit of an arms race.

    Better introspection tooling for Tor is also being integrated into C Tor
    via the \alert{MetricsPort} feature.
\end{frame}

\begin{frame}[plain]
    \tikzset{external/export next=false}

    \begin{tikzpicture}[remember picture, overlay, background rectangle/.style={fill=OnionDarkPurple}, show background rectangle]
        \node[at=(current page.center), align=center] {\includegraphics[width=1\textwidth]{images/metricsport.png}};
    \end{tikzpicture}
\end{frame}

\begin{frame}
    \frametitle{The Tor Network}
    \centering
    \begin{tikzpicture}
        \begin{axis}[
                title=Relay Versions Seen During 2022,
                title style={font=\scriptsize\bfseries},
                no markers,
                enlarge x limits=false,
                grid=both,
                grid style=dashed,
                width=0.85\paperwidth,
                height=0.80\paperheight,
                date coordinates in=x,
                xmin=2022-01-01,
                xmax=2022-07-01,
                xtick={
                    {2022-01-01},
                    {2022-02-01},
                    {2022-03-01},
                    {2022-04-01},
                    {2022-05-01},
                    {2022-06-01},
                    {2022-07-01}
                },
                cycle list name=exotic,
                every axis plot/.append style={thick},
                label style={font=\scriptsize},
                tick label style={font=\scriptsize},
                legend style={
                    font=\tiny,
                },
                legend pos=north west,
                legend cell align=left,
                unbounded coords=discard,
                xticklabel style={
                    anchor=near xticklabel,
                },
                xticklabel=\month\
                ]

              \addlegendentry{0.4.7}
              \addplot table [x=date, y=0.4.7, col sep=comma] {data/versions_compressed.csv};

              \addlegendentry{0.4.6}
              \addplot table [x=date, y=0.4.6, col sep=comma] {data/versions_compressed.csv};

              \addlegendentry{0.4.5 (LTS)}
              \addplot table [x=date, y=0.4.5, col sep=comma] {data/versions_compressed.csv};

              \addlegendentry{0.4.4}
              \addplot table [x=date, y=0.4.4, col sep=comma] {data/versions_compressed.csv};

              \addlegendentry{0.4.3}
              \addplot table [x=date, y=0.4.3, col sep=comma] {data/versions_compressed.csv};

              \addlegendentry{0.4.2}
              \addplot table [x=date, y=0.4.2, col sep=comma] {data/versions_compressed.csv};

              \addlegendentry{0.3.5 (LTS)}
              \addplot table [x=date, y=0.3.5, col sep=comma] {data/versions_compressed.csv};

              \addlegendentry{Other}
              \addplot table [x=date, y=Other, col sep=comma] {data/versions_compressed.csv};

        \end{axis}
    \end{tikzpicture}

    \tiny Source: \href{https://metrics.torproject.org/}{metrics.torproject.org}
\end{frame}

\begin{frame}[plain]
    \tikzset{external/export next=false}

    \begin{tikzpicture}[remember picture, overlay, background rectangle/.style={fill=OnionDarkPurple}, show background rectangle]
        \node[text=white, at=(current page.north), yshift=-2.5cm, font=\bfseries] {A massive \textcolor{lime}{\textbf{thank you}} for upgrading to \textcolor{lime}{\textbf{Tor 0.4.7}} so quickly!};
        \node[at=(current page.center), yshift=-2.5cm, align=center] {\includegraphics[width=0.5\textwidth]{images/fingerprinting.png}};
    \end{tikzpicture}
\end{frame}

\begin{frame}
    \frametitle{Congestion Control}

    Onion Service operators will also benefit from upgrading to \highlight{Tor 0.4.7.}

    For more details, please read Mike Perry's blog post on Congestion
    Control at
    \href{https://blog.torproject.org/congestion-contrl-047/}{blog.torproject.org/congestion-contrl-047}
\end{frame}

\begin{frame}
    \frametitle{Proof of Work for Onion Services}

    Implement PoW for Onion Services that can dynamically enable, disable, and
    adjust the difficulty of the system if pathological situations appears.

    Make the cost of attacking an Onion Service higher.

    A big thanks to \highlight{tevador} for all the help here!

    See \href{https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/327-pow-over-intro.txt}{Proposal
    \#327}.
\end{frame}

\begin{frame}[t]
    \frametitle{Conflux}
    \centering
    \begin{tikzpicture}
        %% Define the style for our relay nodes inside the Anonymity Network cloud.
        \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]
        \tikzstyle{wiretap}=[circle, draw, thin, fill=red!40, scale=0.8]

        %% Alice.
        \node[] at (-6, 2.5) {Alice};
        \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

        %% Bob.
        \node[] at (6, 2.5) {Bob};
        \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

        \node[] at (0, 2) {The Tor Network};
        \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

        %% The relay nodes inside the Anonymity Network cloud.
        \node[relay] (r1) at (-1.9, 0.2)  {$R_{1}$};
        \node[relay] (r2) at (0.0, 0.1)   {$R_{2}$};
        \node[relay] (r3) at (1.8, -0.4)  {$R_{3}$};
        \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
        \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
        \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
        \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

        \path[thick] (r1) edge (r2);
        \path[thick] (r2) edge (r7);
        \path[thick] (r5) edge (r6);
        \path[thick] (r6) edge (r7);

        %% Path between Alice and R1.
        \path[thick] (-4.4, -0.4) edge (r1);

        %% Path between Alice and R5
        \path[thick] (-4.4, -0.4) edge (r5);

        \path[thick] (r7) edge (4.4, -0.4);

        %% Helper lines for debugging.
        %% \draw[help lines] (-7,-3) grid (7,3);
    \end{tikzpicture}
\end{frame}
